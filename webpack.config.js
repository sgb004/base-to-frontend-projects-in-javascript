const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

console.log('PATH', __dirname);

function recursiveIssuer(m) {
	if (m.issuer) {
		return recursiveIssuer(m.issuer);
	} else if (m.name) {
		return m.name;
	} else {
		return false;
	}
}

module.exports = {
	output: {
		path: path.resolve(__dirname, 'public'),
		filename: 'js/[name].js',
		publicPath: '/public/',
	},
	entry: {
		index: './src/js/index.js',
	},
	devServer: {
		port: 8888,
		headers: { 'Access-Control-Allow-Origin': '*' },
	},
	/*
	watch: true,
	watchOptions: {
		ignored: ['public/**', 'node_modules/**', 'src/imgs/**'],
	},*/
	devtool: 'inline-source-map',
	plugins: [
		new HtmlWebpackPlugin({
			filename: './index.html',
			template: './src/index.twig',
			chunks: ['index'],
			hash: true,
		}),
		new MiniCssExtractPlugin({
			filename: 'css/[name].css',
		}),
		//new CopyPlugin({
		//patterns: [
		//{
		//from: '**/*',
		//to: 'imgs/gallery/',
		//context: './src/imgs/gallery/',
		//},
		//],
		//}),
	],
	optimization: {
		minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
		splitChunks: {
			cacheGroups: {
				indexStyles: {
					name: 'index',
					test: (m, c, entry = 'index') =>
						m.constructor.name === 'CssModule' &&
						recursiveIssuer(m) === entry,
					chunks: 'all',
					enforce: true,
				},
			},
		},
	},
	module: {
		rules: [
			{
				test: /\.(jpe?g|png|gif|svg|webp)$/i,
				loader: 'file-loader',
				options: {
					name(resourcePath) {
						resourcePath = resourcePath.split('/src')[1];
						return resourcePath;
					},
				},
			},
			{
				test: /\.twig$/,
				use: [
					//'raw-loader',
					{
						loader: 'html-loader',
						options: {
							attributes: {
								list: [
									{
										tag: 'svg',
										attribute: 'data-src',
										type: 'src',
									},
									{
										tag: 'img-cpt',
										attribute: 'src',
										type: 'src',
									},
									{
										tag: 'link',
										attribute: 'href',
										type: 'src',
									},
									{
										tag: 'section',
										attribute: 'load-background-image',
										type: 'src',
									},
								],
							},
						},
					},
					{
						loader: 'twig-html-loader',
						options: {
							data: {
								url: () => {
									return '/';
								},
							},
						},
					},
				],
			},
			{
				test: /\.css$/,
				use: [MiniCssExtractPlugin.loader, 'css-loader'],
			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				options: {
					presets: [
						[
							'@babel/preset-env',
							{
								targets: {
									browsers: [
										'>=1%',
										'not ie 11',
										'not op_mini all',
									],
								},
								debug: true,
							},
						],
					],
					plugins: ['@babel/plugin-proposal-class-properties'],
				},
			},
		],
	},
};
