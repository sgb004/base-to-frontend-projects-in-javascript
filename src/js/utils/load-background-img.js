/**
 * @name: LoadBackgroundImg
 * @author: sgb004
 * @version: 0.1.0
 */

import { ListenerScroll } from './listener-scroll';
import { elementInViewport } from './element-in-viewport';
import { ImageLoader } from './image-loader';

class LoadBackgroundImg {
	static wait = 1000;
	static lbifn = 'lbiw';
	static imgs = [];

	static loadBackgroundImg() {
		this[this.lbifn]();
	}

	static imgLoad(id) {
		let style = this.imgs[id].element.getAttribute('load-background-style');
		style = style ?? '';
		style += "background-image: url('" + this.imgs[id].img.src + "');";
		this.imgs[id].element.setAttribute('style', style);
		this.imgs[id] = null;
		delete this.imgs[id];
	}

	static lbi() {
		const bi = document.querySelectorAll('*[load-background-image]');
		let img;
		for (let i = 0; i < bi.length; i++) {
			if (elementInViewport(bi[i])) {
				img = new ImageLoader(
					bi[i].getAttribute('load-background-image'),
					this.imgLoad.bind(this),
					this.imgs.length
				);
				img.element = bi[i];
				this.imgs.push(img);
				bi[i].removeAttribute('load-background-image');
			}
		}
		img = undefined;
	}

	static lbiw() {
		let t = () => {
			this.lbi();
			this.lbifn = 'lbi';
		};
		setTimeout(t.bind(this), this.wait);
		this.lbifn = 'lbil';
	}

	static lbil() {}
}

ListenerScroll.addEventListenerScroll(
	LoadBackgroundImg.loadBackgroundImg.bind(LoadBackgroundImg)
);
LoadBackgroundImg.loadBackgroundImg();
