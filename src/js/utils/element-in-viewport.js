/**
 * @link https://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport
 */
/**
 * @link https://stackoverflow.com/a/442474/1919534
 */
function getOffset(el) {
	let _x = 0;
	let _y = 0;
	while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
		_x += el.offsetLeft - el.scrollLeft;
		_y += el.offsetTop - el.scrollTop;
		el = el.offsetParent;
	}
	return { top: _y, left: _x };
}

export function elementInViewport(el) {
	const elOffset = getOffset(el);
	let scrollTop = window.pageYOffset;
	let pt = window.getComputedStyle(el, null).getPropertyValue('padding-top');
	//let pb = window.getComputedStyle(el, null).getPropertyValue('padding-bottom');
	//let elHeight;
	pt = parseFloat(pt.replace(/[^\d.-]/g, ''));
	//pb = parseFloat(pb.replace(/[^\d.-]/g, ''));
	scrollTop -= pt;

	//elHeight = el.offsetHeight - pt - pb;
	scrollTop += window.innerHeight * 0.9;

	return scrollTop >= elOffset.top;
}
