/**
 * @name: ImageUtils
 * @author: sgb004
 * @version: 0.1.0
 */

export class ImageUtils {
	static supportWebP;

	static canUseWebP() {
		if (this.supportWebP === undefined) {
			const elem = document.createElement('canvas');
			this.supportWebP =
				elem.toDataURL('image/webp').indexOf('data:image/webp') == 0;
		}
		return this.supportWebP;
	}
}
