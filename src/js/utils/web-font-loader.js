const webFontLoader = (webFontConfig) => {
	return new Promise((resolve, reject) => {
		const script = document.createElement('script');
		document.body.appendChild(script);
		script.onload = function () {
			// eslint-disable-next-line
			WebFont.load(webFontConfig);
			resolve();
		};
		script.onerror = reject;
		script.async = true;
		script.src =
			'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
	});
};

module.exports = webFontLoader;
