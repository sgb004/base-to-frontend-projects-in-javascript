/**
 * @name: ImageLoader
 * @author: sgb004
 * @version: 0.1.0
 */

export class ImageLoader {
	element;

	constructor(src, listener, id) {
		this.id = id ?? src;
		this.listener = typeof listener == 'function' ? listener : () => {};
		this.img = new Image();
		this.img.src = src;
		this.img.onload = this.loaded.bind(this);
	}

	loaded() {
		this.listener(this.id);
	}
}
