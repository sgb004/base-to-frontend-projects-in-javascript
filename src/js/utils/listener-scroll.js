/**
 * @name: ListenerScroll
 * @author: sgb004
 * @version: 0.1.0
 */

export class ListenerScroll {
	static listenerScrollAdded = false;
	static listenerScroll = [];

	static addEventListenerScroll(fn) {
		this.addListenerScroll();
		this.listenerScroll.push(fn);
		//fn();
	}

	static addListenerScroll() {
		if (!this.listenerScrollAdded) {
			window.addEventListener('scroll', this.eventScroll.bind(this));
			this.listenerScrollAdded = true;
		}
	}

	static eventScroll() {
		for (const fn in this.listenerScroll) {
			if (this.listenerScroll[fn]()) {
				delete this.listenerScroll[fn];
			}
		}
	}
}
