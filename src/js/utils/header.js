import { ListenerScroll } from './listener-scroll';

const headerFixed = () => {
	const header = document.querySelector('header');
	let headerPaddingTop = window
		.getComputedStyle(header, null)
		.getPropertyValue('padding-top');

	headerPaddingTop = parseFloat(headerPaddingTop.replace(/[^\d.-]/g, ''));
	headerPaddingTop -= 5;

	if (headerPaddingTop == 0) {
		headerPaddingTop = header.getAttribute(
			'data-padding-top',
			headerPaddingTop
		);
		headerPaddingTop = parseFloat(headerPaddingTop);
	}

	if (window.pageYOffset > headerPaddingTop) {
		if (!header.classList.contains('header-fixed')) {
			document.body.style.marginTop = header.offsetHeight + 'px';
			document.body.classList.add('header-fixed-actived');
			header.classList.add('header-fixed');
			header.setAttribute('data-padding-top', headerPaddingTop);
		}
	} else {
		document.body.style.marginTop = null;
		document.body.classList.remove('header-fixed-actived');
		header.classList.remove('header-fixed');
	}
};

ListenerScroll.addEventListenerScroll(headerFixed);
headerFixed();
