import { ListenerScroll } from './listener-scroll';
import { elementInViewport } from './element-in-viewport';

const animate = () => {
	const toAnimate = document.querySelectorAll(
		'.animate:not([class*="animate-start"])'
	);
	for (let i = 0; i < toAnimate.length; i++) {
		if (elementInViewport(toAnimate[i])) {
			toAnimate[i].classList.add('animate-start');
			toAnimate[i].classList.add('animated');
		}
	}
};

ListenerScroll.addEventListenerScroll(animate);
animate();
