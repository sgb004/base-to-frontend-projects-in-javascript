import './components/loading-cpt';
import './components/menu-cpt';
import './components/img-cpt';
import './components/iframe-cpt';
import './components/more-btn-cpt';
//import './utils/header';
import './utils/animate';
import './utils/load-background-img';
import 'orange-forms-js';
//import { tns } from 'tiny-slider/src/tiny-slider';
//import { SimpleGallery } from 'simple-gallery-for-js';
import webFontLoader from './utils/web-font-loader';

//DELETE
import hi from './example';

import 'tiny-slider/dist/tiny-slider.css';
import 'simple-gallery-for-js/css/simple-gallery.css';

const indexReady = () => {
	console.log(hi);
};

document.addEventListener('DOMContentLoaded', indexReady);

webFontLoader({
	google: {
		families: ['Montserrat:200,300,400,700,900&display=swap'],
	},
});
