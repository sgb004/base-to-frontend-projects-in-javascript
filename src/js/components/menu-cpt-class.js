/**
 * @name: MenuComponent
 * @author: sgb004
 * @version: 0.1.0
 */

import { LitElement, html } from 'lit-element';

export class MenuComponent extends LitElement {
	createRenderRoot() {
		return this;
	}

	static get properties() {
		return {
			items: { type: Object },
			'aria-label': { type: String },
		};
	}

	constructor() {
		super();
		let idbtn = new Date().getTime();
		idbtn = this.id + '-' + idbtn + '-btn';
		this.items = {};
		this['aria-label'] = 'Go to';
		// prettier-ignore
		this.button = html`<input type="checkbox" class="checkbox-btn" id="${idbtn}"/><label for="${idbtn}" class="menu-main-btn"><span></span><span></span><span></span>Menú</label>`;
	}

	set btn(btn) {
		this.button = btn;
	}

	get btn() {
		return this.button;
	}

	getList(items) {
		let list = html``;
		let itemAttrs, itemAttrsDefault, a, children;

		for (const item in items) {
			if (typeof items[item] == 'object') {
				itemAttrsDefault = {
					href: '',
					target: '',
					title: item,
					id: '',
					class: '',
					'aria-label': this['aria-label'] + ' ' + item,
					html: item,
					children: {},
				};
				children = html``;

				itemAttrs = Object.assign(itemAttrsDefault, items[item]);

				if (typeof itemAttrs.children != 'object') {
					delete itemAttrs.children;
				} else if (Object.keys(itemAttrs.children).length == 0) {
					delete itemAttrs.children;
				} else {
					children = this.getList(itemAttrs.children);
				}
				delete itemAttrs.children;

				a = document.createElement('a');
				a.innerHTML = itemAttrs.html;
				delete itemAttrs.html;
				for (const attr in itemAttrs) {
					if (itemAttrs[attr] !== '') {
						a.setAttribute(attr, itemAttrs[attr]);
					}
				}
				// prettier-ignore
				list = html`${list}<li>${a} ${children}</li>`;
			} else {
				// prettier-ignore
				list = html`${list}<li><a href="${items[item]}" title="${item}" aria-label="${this['aria-label']} ${item}">${item}</a></li>`;
			}
		}
		return html`<ul>
			${list}
		</ul>`;
	}

	render() {
		return html`${this.button}
			<div class="box-closed">${this.getList(this.items)}</div>`;
	}
}
