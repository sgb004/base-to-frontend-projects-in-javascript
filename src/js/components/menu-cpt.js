/**
 * @name: MenuCpt
 * @author: sgb004
 * @version: 0.1.0
 */

import { MenuComponent } from './menu-cpt-class';
import './menu-cpt-style.css';

class MenuCpt extends MenuComponent {
	/*
	constructor() {
		super();
	}
	*/
}

customElements.define('menu-cpt', MenuCpt);
