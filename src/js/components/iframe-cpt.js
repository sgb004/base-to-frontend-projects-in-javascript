/**
 * @name: IframeCpt
 * @author: sgb004
 * @version: 0.1.0
 */

import { LitElement, html, css } from 'lit-element';
import { ListenerScroll } from './../utils/listener-scroll';

class IframeCpt extends LitElement {
	static get styles() {
		// prettier-ignore
		return css`:host {display: flex; align-items: center; position: relative;} loading-cpt{ position: absolute; width: 100%; height: 100%; } iframe { display: block; position: relative; z-index: 1;}`;
	}

	static get properties() {
		return {
			src: { type: String },
			width: { type: String },
			height: { type: String },
		};
	}

	constructor() {
		super();
		this.iframe = null;
		this.loading = document.createElement('loading-cpt');

		ListenerScroll.addEventListenerScroll(this.checkStage.bind(this));
	}

	set src(src) {
		if (this.iframe != null) {
			this.setAttribute('src', src);
			this.iframe.src = src;
		}
	}

	get src() {
		return this.iframe == null ?? this.iframe.getAttribute('src');
	}

	setAttribute(name, value) {
		if (this.iframe != null) {
			name == 'id' || name == 'class'
				? super.setAttribute(name, value)
				: this.iframe.setAttribute(name, value);
		}
	}

	getAttribute(name) {
		return (
			this.iframe == null ??
			(name == 'id' || name == 'class'
				? super.getAttribute(name)
				: this.iframe.getAttribute(name))
		);
	}

	checkStage() {
		//let ett = this.getBoundingClientRect().top - window.innerHeight;
		let ett = this.getBoundingClientRect().top;
		if (window.pageYOffset >= ett) {
			let i, n, v;
			this.iframe = document.createElement('iframe');
			for (i = 0; i < this.attributes.length; i++) {
				n = this.attributes[i].name;
				v = this.attributes[i].value;
				if (n == 'id') {
					v = v + '-child';
				} else if (n == 'class') {
					continue;
				}
				this.iframe.setAttribute(n, v);
			}
			this.iframe.addEventListener('load', this.iframeLoad.bind(this));
			this.requestUpdate();
			return true;
		}
		return false;
	}

	render() {
		return html`${this.loading}${this.iframe}`;
	}

	iframeLoad() {
		const t = function () {
			if (this.loading) {
				this.loading.parentNode.removeChild(this.loading);
			}
			this.loading = null;
		};
		setTimeout(t.bind(this), 2000);
		this.dispatchEvent('added');
	}

	dispatchEvent(event, detail) {
		let e = new CustomEvent(event, { detail });
		this.iframe.dispatchEvent(e);
	}

	addEventListener(type, listener, useCapture) {
		this.iframe.addEventListener(type, listener.bind(this), useCapture);
	}
}

customElements.define('iframe-cpt', IframeCpt);
