/**
 * @name: ImgCpt
 * @author: sgb004
 * @version: 0.1.0
 */

import { LitElement, html, css } from 'lit-element';
import { ImageUtils } from './../utils/image-utils';
import { ListenerScroll } from './../utils/listener-scroll';

class ImgCpt extends LitElement {
	static get styles() {
		// prettier-ignore
		return css`:host {display: inline-block;} img { display: block; width: inherit; height: inherit; max-width: inherit; max-height: inherit;}`;
	}

	static get properties() {
		return {
			src: { type: String },
			alt: { type: String },
			width: { type: String },
			height: { type: String },
		};
	}

	constructor() {
		super();
		this.src = '';
		this.alt = '';
		this.width = '';
		this.height = '';
		this.img = new Image();
		this.img.onload = this.imgLoad.bind(this);
		this.content = '';

		ListenerScroll.addEventListenerScroll(this.checkStage.bind(this));
	}

	imgLoad() {
		this.img.alt = this.alt;
		this.img.setAttribute('width', this.width);
		this.img.setAttribute('height', this.height);
		this.content = this.img;
		this.update();
	}

	checkStage() {
		let ett = this.getBoundingClientRect().top - window.innerHeight;
		if (window.pageYOffset >= ett) {
			this.img.src = ImageUtils.canUseWebP()
				? this.src
				: this.src.substr(0, this.src.lastIndexOf('.')) + '.jpg';
			return true;
		}
		return false;
	}

	render() {
		if (this.content === '') {
			this.checkStage();
			return html`<loading-cpt></loading-cpt>`;
		}
		return html`${this.content}`;
	}
}

customElements.define('img-cpt', ImgCpt);
