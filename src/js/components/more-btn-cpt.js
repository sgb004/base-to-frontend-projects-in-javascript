/**
 * @name: MoreBtnCpt
 * @author: sgb004
 * @version: 0.1.0
 */

import { LitElement, html, css } from 'lit-element';
import { ListenerScroll } from './../utils/listener-scroll';

export class MoreBtnCpt extends LitElement {
	createRenderRoot() {
		return this;
	}

	static get styles() {
		// prettier-ignore
		return css`:host {display: block;}`;
	}

	static get properties() {
		return {};
	}

	constructor() {
		super();

		ListenerScroll.addEventListenerScroll(this.checkPosition.bind(this));
	}

	checkPosition() {
		var so = document.documentElement.scrollTop + window.innerHeight;
		var dh = document.documentElement.offsetHeight;

		if (so >= dh) {
			this.classList.add('bottom');
		} else {
			this.classList.remove('bottom');
		}
		return false;
	}

	clickHandler(e) {
		e.preventDefault();
		var i = this.classList.contains('bottom')
			? 0
			: window.scrollY + window.innerHeight * 0.75;
		window.scrollTo(0, i);
		this.blur();
	}

	render() {
		const btn = document.createElement('button');
		btn.innerHTML = this.innerHTML;
		btn.setAttribute('aria-label', this.getAttribute('aria-label'));
		btn.addEventListener('click', this.clickHandler.bind(this));
		// prettier-ignore
		return html`${btn}`;
	}
}

customElements.define('more-btn-cpt', MoreBtnCpt);
