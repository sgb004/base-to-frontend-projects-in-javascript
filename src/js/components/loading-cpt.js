import { LitElement, html, css } from 'lit-element';

class LoadingCpt extends LitElement {
	static get styles() {
		// prettier-ignore
		return css`svg{ display: block; width: 100%; height: 100%;}`;
	}

	static get properties() {
		return {
			'loading-stroke-color': { type: String },
		};
	}

	constructor() {
		super();
		this['loading-stroke-color'] = '#e15b64';
	}

	render() {
		// prettier-ignore
		return html`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="50" cy="50" fill="none" stroke="${this['loading-stroke-color']}" stroke-width="10" r="35" stroke-dasharray="164.934 56.978"><animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1s" values="0 50 50;360 50 50" keyTimes="0;1"/></circle></svg>`;
	}
}

customElements.define('loading-cpt', LoadingCpt);
